$(function() {
    var  $gallery = $("#gallery");
    $ajax = $.ajax({
     url: 'https://picsum.photos/v2/list',
     method: 'GET',
     dataType: "json",
     success: function(data) {
      $.each(data, function(i, item) {
         $gallery.append(        
             '<div class="col-lg-4 col-md-12 mb-4 mb-lg-0">' +
             '<img src="' + item.download_url + '" class="img-thumbnail rounded" alt="" height="30px" >' +
             '</div>' +
             '</div>'
         );
      });
     },
    });
   });